import React, { Component } from "react";
import Modal from "react-native-modal";
import uuid from "uuid";
import { Button, StyleSheet, Text, View, FlatList } from "react-native";

function Item({ title }) {
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

class HeightResponsiveModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      data: [],
    };
  }

  renderModal() {
    return (
      <Modal
        testID={"modal"}
        isVisible={this.isVisible()}
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}
        onBackdropPress={this.close}
        onSwipeComplete={this.close}
        style={styles.modal}
      >
        <View style={styles.body}>
          <View style={styles.listWrapper}>
            <FlatList
              data={this.state.data}
              renderItem={({ item }) => <Item title={item.title} />}
              keyExtractor={item => item.id}
            />
          </View>
          <View style={styles.footer}>
            <Button
              testID={"close-button"}
              onPress={this.close}
              title="Close"
            />
            <Button testID={"add-button"} onPress={this.add} title="Add" />
            <Button
              testID={"remove-button"}
              onPress={this.remove}
              title="remove"
            />
          </View>
        </View>
      </Modal>
    );
  }

  open = () => this.setState({ visible: true });
  close = () => this.setState({ visible: false, data: [] });
  add = () =>
    this.setState({
      data: [
        ...this.state.data,
        { id: uuid.v4(), title: uuid.v4().substring(0, 7) }
      ]
    });
  remove = () =>
    this.setState({
      data: [...this.state.data.slice(0, this.state.data.length - 1)]
    });
  isVisible = () => this.state.visible;

  renderButton() {
    return (
      <Button testID={"modal-open-button"} onPress={this.open} title="Open" />
    );
  }

  render() {
    return (
      <View style={styles.buttonAndOverlay}>
        {this.renderButton()}
        {this.renderModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonAndOverlay: {
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  body: {
    maxHeight: "50%",
    backgroundColor: "white",
    padding: 10,
    borderRadius: 4,
    margin: 0
  },
  listWrapper: {
    flexShrink: 0.5
  },
  rowContent: {
    fontSize: 20
  },
  item: {
    backgroundColor: "#DDDDDD",
    padding: 5,
    marginVertical: 2
  },
  title: {
    fontSize: 32
  },
  footer: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

export default HeightResponsiveModal;
